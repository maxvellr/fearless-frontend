import React, { useEffect, useState } from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');

    const [startDate, setStartDate] = useState('');

    const [endDate, setEndDate] = useState('');

    const [description, setDescription] = useState('');

    const [presentations, setMaxPresentations] = useState('');

    const [maxAttendees, setMaxAttendees] = useState('');

    const [location, setLocation] = useState('');

    const  handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value)
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_attendees = maxAttendees;
        data.location = location;
        data.max_presentations = presentations;
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setLocation('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxAttendees('');
            setMaxPresentations('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setLocations(data.locations)

        }
      }

      useEffect(() => {
        fetchData();
      }, []);


    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleStartDateChange} placeholder="Start date" value={startDate} required type="date" name="starts" id="starts" className="form-control"/>
                        <label htmlFor="starts">Start date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEndDateChange} placeholder="End date" value={endDate} required type="date" name="ends" id="ends" className="form-control"/>
                        <label htmlFor="ends">End date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleDescriptionChange} placeholder="Description" value={description} required type="textarea" name="description" id="description" className="form-control"/>
                        <label htmlFor="description">Description</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxPresentationsChange} placeholder="Maximum Presentations" value={presentations} required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                        <label htmlFor="max_presentations">Maximum Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxAttendeesChange} placeholder="Maximum Attendees" value={maxAttendees} required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                        <label htmlFor="max_attendees">Maximum Attendees</label>
                    </div>
                    <div className="form mb-3">
                    <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                        <option value="">Select Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.name} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    </div>
            </div>
        </div>

    );
}

export default ConferenceForm
